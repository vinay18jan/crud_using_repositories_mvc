﻿using MVCForms.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MVCForms.Repository
{
    public class FormRepository
    {
        private SqlConnection con;
        //To Handle connection related activities    
        private void connection()
        {
            //string constr = ConfigurationManager.ConnectionStrings["FormsDB"].ToString();
            con = new SqlConnection("Data Source=.\\sqlexpress;Initial Catalog=sdvvds;Integrated Security=SSPI;");
        }    

        //to add employees
        public bool AddNew(FormsModel model)
        {
            connection();
            SqlCommand cmd = new SqlCommand("sp_insert", con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id",model.Id);
            cmd.Parameters.AddWithValue("@Name",model.Name);
            cmd.Parameters.AddWithValue("@Email", model.Email);
            cmd.Parameters.AddWithValue("@Gender", model.Gender);
            cmd.Parameters.AddWithValue("@DOB", DateTime.Now);
            cmd.Parameters.AddWithValue("@Pswd", model.Pswd);
            cmd.Parameters.AddWithValue("@RetyPswd", model.RetyPswd);
            cmd.Parameters.AddWithValue("@Home_Address", model.Home_Address);
            cmd.Parameters.AddWithValue("@Office_Address", model.Office_Address);
            cmd.Parameters.AddWithValue("@Mobile_Number", model.Mobile_Number);
            cmd.Parameters.AddWithValue("@Phone_Number", model.Phone_Number);
            cmd.Parameters.AddWithValue("@State", model.State);
            cmd.Parameters.AddWithValue("@City", model.City);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
                return true;
            else
                return false;
        }

        //to view all the details with list
        public List<FormsModel> GetAllData()
        {
            connection();
            List<FormsModel> FormList = new List<FormsModel>();
            SqlCommand cmd = new SqlCommand("sp_Selectform", con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            adp.Fill(dt);
            con.Close();

            //bind FormsModel list using dataRow
            foreach (DataRow dr in dt.Rows)
            {
                FormList.Add(new FormsModel 
                {
                    Id = Convert.ToInt32(dr["Id"]),
                    Name = Convert.ToString(dr["Name"]),
                    Email = Convert.ToString(dr["Email"]),
                    Home_Address = Convert.ToString(dr["Home_Address"]),
                    Office_Address = Convert.ToString(dr["Office_Address"]),
                    Mobile_Number = Convert.ToInt32(dr["Mobile_Number"]),
                    Phone_Number = Convert.ToInt32(dr["Phone_Number"]),
                    State = Convert.ToString(dr["State"]),
                    City = Convert.ToString(dr["City"])
                });
            }
            return FormList;
        }

        //update form data
        public bool UpdateData(FormsModel model)
        {
            connection();
            SqlCommand cmd = new SqlCommand("sp_UpdateFormData", con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", model.Id);
            cmd.Parameters.AddWithValue("@Name", model.Name);
            cmd.Parameters.AddWithValue("@Email", model.Email);
            cmd.Parameters.AddWithValue("@Gender", model.Gender);
            cmd.Parameters.AddWithValue("@DOB", DateTime.Now);
            cmd.Parameters.AddWithValue("@Pswd", model.Pswd);
            cmd.Parameters.AddWithValue("@RetyPswd", model.RetyPswd);
            cmd.Parameters.AddWithValue("@Home_Address", model.Home_Address);
            cmd.Parameters.AddWithValue("@Office_Address", model.Office_Address);
            cmd.Parameters.AddWithValue("@Mobile_Number", model.Mobile_Number);
            cmd.Parameters.AddWithValue("@Phone_Number", model.Phone_Number);
            cmd.Parameters.AddWithValue("@State", model.State);
            cmd.Parameters.AddWithValue("@City", model.City);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
                return true;
            else
                return false;
        }

        //delete
        public bool DeleteData(int Id)
        {
            connection();
            SqlCommand cmd = new SqlCommand("sp_DeleteData", con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id",Id);
            con.Open();
            int i= cmd.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
                return true;
            else
                return false;
        }
    }
}