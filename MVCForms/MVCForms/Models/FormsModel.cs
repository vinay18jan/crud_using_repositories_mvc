﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCForms.Models
{
    public class FormsModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please Enter Email Id")]
        public string Email { get; set; }

        public Genders Gender { get; set; }
        public DateTime DOB { get; set; }

        [Required(ErrorMessage = "Password is Required")]
        public string Pswd { get; set; }

        public string RetyPswd { get; set; }

        [Required(ErrorMessage = "Enter Home Address")]
        public string Home_Address { get; set; }

        [Required(ErrorMessage = "Enter Office Address")]
        public string Office_Address { get; set; }

        [Required(ErrorMessage = "Enter Mobile Number")]
        public decimal Mobile_Number { get; set; }

        [Required(ErrorMessage = "Enter Phone Number")]
        public decimal Phone_Number { get; set; }


        public string State { get; set; }
        public string City { get; set; }
        public enum Genders
        {
            Male,
            Female
        }
    }
}