﻿using MVCForms.Models;
using MVCForms.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCForms.Controllers
{
    public class FormController : Controller
    {
        //get: all data
        public ActionResult GetAllFormData(int id)
        {
            FormRepository repo = new FormRepository();
            ModelState.Clear();
            return View(repo.GetAllData().Where(x=>x.Id==id));
        }

        public ActionResult Select(int? Id)
        {
            FormRepository repo = new FormRepository();
            ModelState.Clear();
            var result = repo.GetAllData();
            return View(result);
        }
       
        //insert
        public ActionResult Insert()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Insert(FormsModel model)
        {
            FormRepository repo = new FormRepository();
            if (repo.AddNew(model))
            {
                ViewBag.AddNewSuccessFull = "Details Added Successfully";
            }
            return View();
        }

        //Delete
        public ActionResult Delete(int Id)
        {
            FormRepository repo = new FormRepository();
            if (repo.DeleteData(Id))
                return RedirectToAction("Select");
            else
                return View();
        }

        //GET: update
        public ActionResult Update(int id)
        {
            FormRepository repo = new FormRepository();

            var result = repo.GetAllData().Find(x=>x.Id==id);
            return View(result);
        }

       //Post: Update
        [HttpPost]
        public ActionResult Update(int Id, FormsModel model)
        {
            FormRepository repo = new FormRepository();
            repo.UpdateData(model);
            return RedirectToAction("Select");
        }
    }
}
